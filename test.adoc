A simple sequence diagram:

[plantuml,"test1",png]
----
Bob -> Alice : hello
Alice -> Bob : Go Away
----

a component diagram:

[plantuml,"test2",png]
----
[Bob] -> [Alice] : hello
[Alice] -> [Bob] : Go Away
----

now both as svg:


A simple sequence diagram:

[plantuml,"test3",svg]
----
Bob -> Alice : hello
Alice -> Bob : Go Away
----

a component diagram:

[plantuml,"test4",svg]
----
[Bob] -> [Alice] : hello
[Alice] -> [Bob] : Go Away
----

now a mindmap:

[plantuml,"test5",svg]
----
@startmindmap
* center
** though 1
** thought 2
***_ thought 3

-- though 4
@endmindmap
----
