A simple sequence diagram:

```plantuml
Bob -> Alice : hello
Alice -> Bob : Go Away
```

a component diagram:

```plantuml
[Bob] -> [Alice] : hello
[Alice] -> [Bob] : Go Away
```